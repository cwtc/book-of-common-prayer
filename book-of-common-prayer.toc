\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Setup}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Fonts}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Music}{2}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Page setup}{2}{subsection.2.3}%
\contentsline {section}{\numberline {3}Documentation}{2}{section.3}%
\contentsline {subsection}{\numberline {3.1}Special symbols}{2}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Special commands}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Text formatting}{3}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Instructions}{3}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Bible verses}{3}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}Miscellaneous}{4}{subsubsection.3.3.3}%
\contentsline {subsection}{\numberline {3.4}Environments}{5}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Named responses with \lstinline {responses}}{5}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Versicle-response responses with \lstinline {vresponses}}{5}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}Doubled versicle-response responses with \lstinline {vresponsesdouble}}{6}{subsubsection.3.4.3}%
\contentsline {subsubsection}{\numberline {3.4.4}Prayers with \lstinline {prayer}}{6}{subsubsection.3.4.4}%
\contentsline {subsubsection}{\numberline {3.4.5}Two-column prayers with \lstinline {twocolprayer}}{6}{subsubsection.3.4.5}%
\contentsline {subsubsection}{\numberline {3.4.6}Three-column prayers with \lstinline {threecolprayer}}{7}{subsubsection.3.4.6}%
\contentsline {subsubsection}{\numberline {3.4.7}Psalm typesetting}{7}{subsubsection.3.4.7}%
